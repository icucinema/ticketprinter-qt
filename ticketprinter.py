#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# An AMAZING Qt-powered touchscreen ticket printing piece of awesome.
# 
# By Legs. Fixed by George!

import sys
import sqlite3
import subprocess
import os, string, random, serial, ConfigParser, logging, time, datetime
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtSql import QSqlQueryModel,QSqlDatabase,QSqlQuery

from ehack import eHack # eActivities library, required for getting member reports, purchase reports

from import_members import import_members

# Where are we?
home = os.path.expanduser("~")
path_prefix = home + "/cinema/ticketprinter-qt/"



# This is how the config is parsed. It needs to be here so all the settings can be read
c_filename = "films.cfg"


def GetConfig(section, item, itemtype=None):
    config = ConfigParser.ConfigParser()
    config.read(path_prefix + c_filename)
    if itemtype == 'float':
        #print 'floaty'
        value = config.getfloat(section, item)
        #print value
        return value
    elif itemtype == 'int':
        return config.getint(section, item)
    else: # string
        return config.get(section, item)
    
def SetConfig(new_config):
    #print new_config
    config = ConfigParser.ConfigParser()
    config.read(path_prefix + c_filename)
    for section in new_config.items():
        #print section[0]
        for item, value in section[1].items():
            #print item, value
            config.set(section[0], item, value)
    
    with open(path_prefix + c_filename, 'wb') as configfile:
        config.write(configfile)

    
    return 0



# Serial port & printer setup

serial_port_device = GetConfig("printer", "port")
serial_port = serial.Serial(port=serial_port_device, baudrate=GetConfig("printer", "baudrate", 'int'))
# Further testing has shown that this actully works with both printers. Woo!

printer_type = GetConfig("printer", "printer") # Choice of 'basic', 'ibm' and 'cbm' currently


# What is today?

today = datetime.date.today()
today_day = today.strftime("%d")
today_month = today.strftime("%b")
today_long = today.strftime("%A %d %b %Y")


# Initialise logger

logger = logging.getLogger('ticketlog')
hldr = logging.FileHandler( path_prefix + 'logs/log-' + str(today) + '.log')
formatter = logging.Formatter('%(asctime)s %(message)s')
hldr.setFormatter(formatter)
logger.addHandler(hldr)
logger.setLevel(logging.INFO)
logger.info('Starting up...')

# SQLite3 connection
sql_conn = sqlite3.connect(path_prefix + GetConfig("database","filename"))



def cash_drawer_open():
    # Just opens the drawer. Does nothing special.
    serial_port.write(chr(0x1B)+chr(0x70)+chr(0x02)+chr(0x1F)+chr(0x0F))

def generate_printout(type, vars):
    
    with open(path_prefix + 'templates/' + type + '.txt', 'r') as content_file:
        content = string.Template(content_file.read())
    ticket = content.substitute(vars)
    return ticket
    

    
def print_ticket(this_film, ticket_type, number):
    # this_film is a STRING which matches an entry in films.cfg
    # Usual suspects: film1, film2, doublebill
    # CASE MATTERS (Use CamelCaps)
  
    # ticket_type is a STRING
    # m = member ticket
    # n = Non-Member ticket
    # NB this is LOWERCASE 
    
    # number is an integer, representing the ticket number
    
    # Generate ticket ID
    this_film_title = GetConfig(this_film, 'title')
    t_id = random.choice(this_film_title).upper() + today_day + '-'
    for word in this_film_title.split():
        t_id = t_id + str(len(word))
    t_id = t_id + '-' + str(len(this_film_title.split()))
    
    
    new_ticket = generate_printout(printer_type, dict(header=GetConfig(this_film,'showing') + str(number).zfill(3),
                   ticket_head= GetConfig('general','cinema') + ' Presents',
                   film_title=GetConfig(this_film,'title'),
                   film_line2='',
                   film_line3='',
                   date=today_day + ' ' + today_month,
                   time=GetConfig(this_film,'time'),
                   type=GetConfig(this_film,ticket_type + 'string'),
                   price=GetConfig(this_film,ticket_type + 'price'),
                   id=t_id,
                   number=str(number).zfill(3),
                   website=GetConfig('general','website'),
                   tagline=GetConfig('general','tagline') ))
    
    
    
    
    # New style tickets with big numbers
    serial_port.write(new_ticket)
    
    log_ticket = generate_printout('log', dict(header=GetConfig(this_film,'showing') + str(number).zfill(3),
                   ticket_head= '',
                   film_title=GetConfig(this_film,'title'),
                   date=today_day + ' ' + today_month,
                   time=GetConfig(this_film,'time'),
                   type=GetConfig(this_film,ticket_type + 'string'),
                   price=GetConfig(this_film,ticket_type + 'price'),
                   id=t_id,
                   number=str(number).zfill(3),
                   cid = ''))
    
    
    logger.info('Printing ticket')
    logger.info(log_ticket)
    


# Classes!

class OpenAboutWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("about.ui", self)

class OpenReportWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("reportwindow.ui", self)
  
    
class TheConfigWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("config.ui", self)
  def commit_changes(self):
    print "New films"
    
class TheCorrectionsWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("corrections.ui", self)

class TheAdminWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("admin.ui", self)
    
  def on_update_view_clicked(self, checked=None):
    if checked is None: return
    file_name = path_prefix + GetConfig('database', 'filename')
    db = QSqlDatabase.addDatabase("QSQLITE")
    db.setDatabaseName(file_name)
    db.open()

    projectModel = QSqlQueryModel()
    projectModel.setQuery("select * from members",db)

    projectView = self.ui.memberViewer
    projectView.setModel(projectModel)
    projectView.show()
    db.close()


class TheUpdateWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    self.reportbox = None
    QtGui.QWidget.__init__(self, None)
    self.ui = uic.loadUi("eactivitieslogin.ui", self)
    
    QtCore.QObject.connect(self.ui.pB_update,QtCore.SIGNAL("clicked()"), self.download_new_members)
  def download_new_members(self):
    username = self.ui.lE_username.text()
    password = self.ui.lE_password.text()
    self.ui.lE_password.setText("")
    self.ui.lE_username.setText("")
    
    if self.reportbox is None:
        self.reportbox = OpenReportWindow(self)
    self.reportbox.ui.tE_report.setText("Running...\n")
    self.reportbox.show()
    
    newmembers = eHack(username, password, "/admin/csp/details/411", [3, 395], 1700, "csv", "Members_report")
    password = None
    #print newmembers
    
    out = import_members(sql_conn, newmembers)
    sql_conn.commit()


    self.reportbox.ui.tE_report.setText(out)
    self.reportbox.ui.tE_report.moveCursor(QtGui.QTextCursor.End)
    self.hide()
    
    
    
class MemberChecker(QtGui.QDialog):
  def __init__(self, parent=None):
    QtGui.QDialog.__init__(self, None)
    self.ui = uic.loadUi("membership.ui", self)
    
    self.check_free = False
    
    QtCore.QObject.connect(self.ui.pB_swipe,QtCore.SIGNAL("clicked()"), self.check_swipe)
    QtCore.QObject.connect(self.ui.pB_CID,QtCore.SIGNAL("clicked()"), self.link_data)
    QtCore.QObject.connect(self.ui.pB_override,QtCore.SIGNAL("clicked()"), self.override)
    QtCore.QObject.connect(self.ui.pB_cancel,QtCore.SIGNAL("clicked()"), self.cancel)
  
  # Functions below are button handlers. Return 1 for member, no free tickets, 2 for 
  # member with a free ticket.
  
  # Handle members not in the database _before_ returning.
  
  def check_swipe(self):
    c = sql_conn.cursor()
    
    swipe_number = str(self.ui.lE_swipe.text())
    #print swipe_number
    if swipe_number[0] == ';':
      swipe_number = swipe_number[1:-1] #Trim the crap off the ends
    print swipe_number
    
    #try:
    results = c.execute('select * from members where swipecard=\'' + swipe_number + '\' or CID=\'' + swipe_number + '\' or login=\'' + swipe_number + '\'').fetchall()
    if results:
        # Making the assumption that nobody EVER shares CID, swipe no. or login.
	print "Match"
	print self.check_free
	if (self.check_free):
	    # Check if first ticket
	    self.first_ticket()
	    print "checking first ticket"
	else:
	    # Don't check, not asked.
	    self.done(1)

    else:
	print "No match"

	#Enable boxes for linking
	print "New member, no swipe"
	self.ui.label_3.setEnabled(True)
	self.ui.lE_CID.setEnabled(True)
	self.ui.pB_CID.setEnabled(True)
	self.ui.lE_CID.setFocus(True)

    
  def first_ticket(self):
    swipe_number = str(self.ui.lE_swipe.text())
    #print swipe_number
    if swipe_number[0] == ';':
      swipe_number = swipe_number[1:-1] #Trim the crap off the end
    print swipe_number
    
    c = sql_conn.cursor()
    # Look for an entry in the database
    results = c.execute('select * from members where swipecard=\'' + swipe_number + '\' or CID=\'' + swipe_number + '\' or login=\'' + swipe_number + '\'').fetchall()
    if (results[0][-2] == None) or (results[0][-2] == ""):
      if str(self.sender().objectName()).find("2") == -1:
        film_used = GetConfig("film2",'title') + ', ' + today_long + ', ' + GetConfig("film2",'time')
      else:
	film_used = GetConfig("film1",'title') + ', ' + today_long + ', ' + GetConfig("film1",'time')
      
      c.execute('update members set free_ticket_used_for = \'' + str(film_used) + '\' where swipecard=\'' + swipe_number + '\' or CID=\'' + swipe_number + '\' or login=\'' + swipe_number + '\'')
      sql_conn.commit()
      first_t = True
    else:
      first_t = False
    
    if first_t:
      self.done(2)
    else:
      self.done(1)
    
  def override(self):
      self.done(3)
    
  def link_data(self):
    c = sql_conn.cursor()
    cid_number = str(self.ui.lE_CID.text())
    swipe_number = str(self.ui.lE_swipe.text())
    #print swipe_number
    if swipe_number[0] == ';':
      swipe_number = swipe_number[1:-1] #Trim the crap off the end
    print swipe_number
    
    
    results = c.execute('select * from members where CID=\'' + str(cid_number) + '\'').fetchall()
    if results:
      #Add card number
      c.execute('update members set swipecard = \'' + str(swipe_number) + '\' where CID=\'' + str(cid_number) + '\'')
      sql_conn.commit()
      
      print "added"
      if self.check_free:
	self.first_ticket()
	print "checking first ticket"
      else:
	self.done(1)
    else:
      self.done(0) # Not actually a member!
    
    
  def cancel(self):
    # Return 0, no tickets.
    
    self.done(0)
    
class StartMainGUI(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = uic.loadUi("gui_print2.ui", self)
        
        self.aboutWindow = None
        self.adminWindow = None
        self.configWindow = None
        self.memberWindow = None
        self.updateWindow = None
        self.ellenWindow = None
        
        # Initialise counters

        self.ticket_cnt_total = 0
        self.ticket_cnt_season = 0
        
        self.ticket_cnt_f1_mem = 0
        self.ticket_cnt_f1_nonm = 0
        self.ticket_cnt_f1_free = 0
        
        self.ticket_cnt_f2_mem = 0
        self.ticket_cnt_f2_nonm = 0
        self.ticket_cnt_f2_free = 0
        
        self.ticket_cnt_db_mem = 0
        self.ticket_cnt_db_nonm = 0
        self.ticket_cnt_db_free = 0
        
        self.ticket_cnt_season_f1 = 0
        self.ticket_cnt_season_f2 = 0
        self.ticket_cnt_season_door = 0
        
        self.people_cnt_film1 = 0
        self.people_cnt_film2 = 0
        
        self.ticket_price_non_mem_f1 = GetConfig("film1",'nprice')
        self.ticket_price_non_mem_f2 = GetConfig("film2",'nprice')
        self.ticket_price_mem_free = 0
        self.ticket_price_dbl_member = GetConfig("doublebill",'mprice')
        self.ticket_price_dbl_non_mem = GetConfig("doublebill",'nprice')
        self.ticket_price_dbl_free = GetConfig("doublebill",'fprice')
        self.ticket_price_season = GetConfig('seasondoor','mprice')
        
        self.ui.pB_film1_mem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\nMember price\n\n£" + str(GetConfig("film1",'mprice')), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_film1_nonmem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\nNon-Member price\n\n£" + str(self.ticket_price_non_mem_f1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_film1_mem_first.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\n(Member, first film, FREE)\n\n£" + str(self.ticket_price_mem_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.pB_film2_mem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\nMember price\n\n£"+str(GetConfig("film2",'mprice')), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_film2_nonmem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\nNon-Member price\n\n£" + str(self.ticket_price_non_mem_f2), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_film2_mem_first.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\n(Member, first film, FREE)\n\n£" + str(self.ticket_price_mem_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.pB_filmd_mem.setText(QtGui.QApplication.translate("Form", "Double Bill\n\nMember price\n\n£"+str(self.ticket_price_dbl_member), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_filmd_nonmem.setText(QtGui.QApplication.translate("Form", "Double Bill\n\nNon-Member price\n\n£" + str(self.ticket_price_dbl_non_mem), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.pB_filmd_mem_first.setText(QtGui.QApplication.translate("Form", "Double Bill\n\n(Member, first film, FREE)\n\n£" + str(self.ticket_price_dbl_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.pB_season_door.setText(QtGui.QApplication.translate("Form", "Buy season ticket\n\non the door\n\n(Members only)\n\n£" + str(self.ticket_price_season), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Starting up")
        self.ui.logbox.moveCursor(QtGui.QTextCursor.End)
        
        # here we connect signals with our slots
        
        # No sale = just open the drawer
        QtCore.QObject.connect(self.ui.pB_nosale,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        
        
        # First three buttons. All are paid, so all require both tickets printed and cash drawers open
        #
        # Cash drawer opening functions.
        QtCore.QObject.connect(self.ui.pB_film1_mem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        QtCore.QObject.connect(self.ui.pB_film2_mem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        QtCore.QObject.connect(self.ui.pB_filmd_mem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        #----------------------------------
        # Second three buttons. All paid, so still require cashg drawer
        #
        # Cash drawer opening functions.
        QtCore.QObject.connect(self.ui.pB_film1_nonmem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        QtCore.QObject.connect(self.ui.pB_film2_nonmem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        QtCore.QObject.connect(self.ui.pB_filmd_nonmem,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        #----------------------------------
        # Third set of buttons. First two options are free, so cash drawer does not open.
        # Third option is to purchase a double bill ticket, using the free ticket for part payment. This
        # means the cash drawer does need to be opened.
        QtCore.QObject.connect(self.ui.pB_filmd_mem_first,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        #----------------------------------
        # Lastly, season ticket options.
        #The third button (Buy season ticket on the door) does require cash drawer opening.
        QtCore.QObject.connect(self.ui.pB_season_door,QtCore.SIGNAL("clicked()"), self.cash_drawer_open)
        
    def closeEvent(self, event):
        
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure you want to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.on_pB_report_clicked(checked=True)
            event.accept()
        else:
            event.ignore()
        
    def cash_drawer_open(self):
        # Just opens the drawer. Does nothing special.
        serial_port.write(chr(0x1B)+chr(0x70)+chr(0x02)+chr(0x1F)+chr(0x0F))

    def on_pB_admin_clicked(self, checked=None):
        if checked is None: return
        if self.adminWindow is None:
            self.adminWindow = TheAdminWindow(self)
        self.adminWindow.show()
        
        
        
    def on_pB_nosale_clicked(self,checked=None):
        if checked is None: return
        # Log any cash drawer accesses.
        self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": NO SALE")
    def on_pB_About_clicked(self,checked=None):
        if checked is None: return
        # Make about window open here
        if self.aboutWindow is None:
            self.aboutWindow = OpenAboutWindow(self)
        self.aboutWindow.show()
    
    def on_pB_members_update_clicked(self,checked=None):
        if checked is None: return
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.')
        print filename
        if filename != "":
	  f = open(filename, 'r')
          out = import_members(sql_conn, f.read())
          sql_conn.commit()
	  
    def on_pB_members_update_login_clicked(self,checked=None):
        if checked is None: return
        if self.updateWindow is None:
	    self.updateWindow = TheUpdateWindow(self)
	self.updateWindow.show()
        
    def on_pB_fix_Ellen_clicked(self,checked=None):
        if checked is None: return
        if self.ellenWindow is None:
            self.ellenWindow = TheCorrectionsWindow(self)
        self.ellenWindow.show()
        
        
        
        self.ellenWindow.ui.film1_membox.setValue(self.ticket_cnt_f1_mem)
        self.ellenWindow.ui.film1_nonmbox.setValue(self.ticket_cnt_f1_nonm)
        self.ellenWindow.ui.film1_freebox.setValue(self.ticket_cnt_f1_free)
        
        self.ellenWindow.ui.film2_membox.setValue(self.ticket_cnt_f2_mem)
        self.ellenWindow.ui.film2_nonmbox.setValue(self.ticket_cnt_f2_nonm)
        self.ellenWindow.ui.film2_freebox.setValue(self.ticket_cnt_f2_free)
        
        self.ellenWindow.ui.filmd_membox.setValue(self.ticket_cnt_db_mem)
        self.ellenWindow.ui.filmd_nonmbox.setValue(self.ticket_cnt_db_nonm)
        self.ellenWindow.ui.filmd_freebox.setValue(self.ticket_cnt_db_free)
        
        self.ellenWindow.ui.film1_seasonbox.setValue(self.ticket_cnt_season_f1)
        self.ellenWindow.ui.film2_seasonbox.setValue(self.ticket_cnt_season_f2)
        self.ellenWindow.ui.season_doorbox.setValue(self.ticket_cnt_season_door)
        
        
        
        QtCore.QObject.connect(self.ellenWindow.ui.film1_nonmbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f1n)
        QtCore.QObject.connect(self.ellenWindow.ui.film1_membox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f1m)
        QtCore.QObject.connect(self.ellenWindow.ui.film1_freebox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f1f)
        QtCore.QObject.connect(self.ellenWindow.ui.film1_seasonbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f1s)
        
        QtCore.QObject.connect(self.ellenWindow.ui.film2_nonmbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f2n)
        QtCore.QObject.connect(self.ellenWindow.ui.film2_membox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f2m)
        QtCore.QObject.connect(self.ellenWindow.ui.film2_freebox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f2f)
        QtCore.QObject.connect(self.ellenWindow.ui.film2_seasonbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_f2s)
        
        QtCore.QObject.connect(self.ellenWindow.ui.filmd_nonmbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_fdn)
        QtCore.QObject.connect(self.ellenWindow.ui.filmd_membox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_fdm)
        QtCore.QObject.connect(self.ellenWindow.ui.filmd_freebox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_fdf)
        QtCore.QObject.connect(self.ellenWindow.ui.season_doorbox,QtCore.SIGNAL("valueChanged(int)"),self.update_counts_fsd)
        
    def update_counts_f1n(self):
        #print "Update counts"
        self.ticket_cnt_f1_nonm = self.ellenWindow.ui.film1_nonmbox.value()
        
        self.ui.ct_film1_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f1_nonm), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f1m(self):
        #print "Update counts"
        self.ticket_cnt_f1_mem = self.ellenWindow.ui.film1_membox.value()
        
        self.ui.ct_film1_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f1_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f1f(self):
        #print "Update counts"
        self.ticket_cnt_f2_free = self.ellenWindow.ui.film2_freebox.value()
        
        self.ui.ct_film2_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f2n(self):
        #print "Update counts"
        self.ticket_cnt_f2_nonm = self.ellenWindow.ui.film2_nonmbox.value()
        
        self.ui.ct_film2_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_nonm), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f2m(self):
        #print "Update counts"
        self.ticket_cnt_f2_mem = self.ellenWindow.ui.film2_membox.value()
        
        self.ui.ct_film2_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f2f(self):
        #print "Update counts"
        self.ticket_cnt_f2_free = self.ellenWindow.ui.film2_freebox.value()
        
        self.ui.ct_film2_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_fdn(self):
        #print "Update counts"
        self.ticket_cnt_db_nonm = self.ellenWindow.ui.filmd_nonmbox.value()
        
        self.ui.ct_db_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_nonm), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_fdm(self):
        #print "Update counts"
        self.ticket_cnt_db_mem = self.ellenWindow.ui.filmd_membox.value()
        
        self.ui.ct_db_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_fdf(self):
        #print "Update counts"
        self.ticket_cnt_db_free = self.ellenWindow.ui.filmd_freebox.value()
        
        self.ui.ct_db_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_free), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f1s(self):
        #print "Update counts"
        self.ticket_cnt_season_f1 = self.ellenWindow.ui.film1_seasonbox.value()
        
        self.ui.ct_season1.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_f1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_f2s(self):
        #print "Update counts"
        self.ticket_cnt_season_f2 = self.ellenWindow.ui.film2_seasonbox.value()
        
        self.ui.ct_season2.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_f2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

    def update_counts_fsd(self):
        #print "Update counts"
        self.ticket_cnt_season_door = self.ellenWindow.ui.season_doorbox.value()
        
        self.ui.ct_season_door.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_door), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))

        

    def update_counts(self):
       
        self.ticket_cnt_total = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
                                
        self.ticket_cnt_season = self.ticket_cnt_season_f1 + self.ticket_cnt_season_f2 # Running tally of season tickets USED only.
        
        self.people_cnt_film1 = self.ticket_cnt_f1_mem + self.ticket_cnt_f1_nonm + self.ticket_cnt_f1_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f1
        
        self.people_cnt_film2 = self.ticket_cnt_f2_mem + self.ticket_cnt_f2_nonm + self.ticket_cnt_f2_free + self.ticket_cnt_db_mem + self.ticket_cnt_db_nonm + self.ticket_cnt_db_free + self.ticket_cnt_season_f2
        
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
    
    def on_pB_report_clicked(self,checked=None):
        if checked is None: return
        self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Printing report")
        self.ui.logbox.moveCursor(QtGui.QTextCursor.End)
        report_template = string.Template('\
Imperial Cinema\n\n\
Ticket sales report for ' + today_long + '\n\n\
\
Film 1:' + GetConfig("film1",'title') + '\n\
Film 2:' + GetConfig("film2",'title') + '\n\
\n\
------------Counts------------------\n\
Type | Film 1 | Film 2 | Double Bill\n\
-----|--------|--------|------------\n\
Non-M|' + str(self.ticket_cnt_f1_nonm).rjust(8) + '|' + str(self.ticket_cnt_f2_nonm).rjust(8) + '|' + str(self.ticket_cnt_db_nonm).rjust(8) + '    \n\
Mem-P|' + str(self.ticket_cnt_f1_mem).rjust(8) + '|' + str(self.ticket_cnt_f2_mem).rjust(8) + '|' + str(self.ticket_cnt_db_mem).rjust(8) + '    \n\
Mem-F|' + str(self.ticket_cnt_f1_free).rjust(8) + '|' + str(self.ticket_cnt_f2_free).rjust(8) + '|' + str(self.ticket_cnt_db_free).rjust(8) + '    \n\
Seas.|' + str(self.ticket_cnt_season_f1).rjust(8) + '|' + str(self.ticket_cnt_season_f2).rjust(8) + '|OTD:' +str(self.ticket_cnt_season_door).rjust(4) +'    \n\
------------------------------------\n\
\n\
-------------Take-------------------\n\
Type | Film 1 | Film 2 | Double Bill\n\
-----|--------|--------|------------\n\
Non-M|${f1_nm_s}|${f2_nm_s}|${db_nm_s}    \n\
Mem-P|${f1_mp_s}|${f2_mp_s}|${db_mp_s}    \n\
Mem-F|${f1_mf_s}|${f2_mf_s}|${db_mf_s}    \n\
------------------------------------\n\
\n\
Expected take film 1 :    GBP ${f1_tot_t} \n\
Expected take film 2 :    GBP ${f2_tot_t} \n\
Expected take season :    GBP ${sea_tot_t}\n\
Expected take total  :    GBP ${f_tot_t}\n\n')

        f1_nm_t = float(self.ticket_cnt_f1_nonm) * GetConfig("film1",'nprice', 'float')
        f1_nm_s = str(f1_nm_t).rjust(8)
        f2_nm_t = float(self.ticket_cnt_f2_nonm) * GetConfig("film2",'nprice', 'float')
        f2_nm_s = str(f2_nm_t).rjust(8)
        db_nm_t = float(self.ticket_cnt_db_nonm) * GetConfig("doublebill",'nprice', 'float')
        db_nm_s = str(db_nm_t).rjust(8)
        
        f1_mp_t = float(self.ticket_cnt_f1_mem) * GetConfig("film1",'mprice', 'float')
        f1_mp_s = str(f1_mp_t).rjust(8)
        f2_mp_t = float(self.ticket_cnt_f2_mem) * GetConfig("film2",'mprice', 'float')
        f2_mp_s = str(f2_mp_t).rjust(8)
        db_mp_t = float(self.ticket_cnt_db_mem) * GetConfig("doublebill",'mprice', 'float')
        db_mp_s = str(db_mp_t).rjust(8)
        
        f1_mf_t = float(self.ticket_cnt_f1_free) * float(self.ticket_price_mem_free)
        f1_mf_s = str(f1_mf_t).rjust(8)
        f2_mf_t = float(self.ticket_cnt_f2_free) * float(self.ticket_price_mem_free)
        f2_mf_s = str(f2_mf_t).rjust(8)
        db_mf_t = float(self.ticket_cnt_db_free) * float(self.ticket_price_dbl_free)
        db_mf_s = str(db_mf_t).rjust(8)
        
        f1_sea_t = float(self.ticket_cnt_season_f1) * 0
        f1_sea_s = str(f1_sea_t).rjust(8)
        f2_sea_t = float(self.ticket_cnt_season_f2) * 0
        f2_sea_s = str(f2_sea_t).rjust(8)
        
        
        
        f1_tot_t  = f1_nm_t + f1_mp_t + f1_mf_t + (db_mp_t * 0.5) + (db_nm_t * 0.5) + (db_mf_t * 0.5)
        f2_tot_t  = f2_nm_t + f2_mp_t + f2_mf_t + (db_mp_t * 0.5) + (db_nm_t * 0.5) + (db_mf_t * 0.5)
        sea_tot_t = float(self.ticket_cnt_season_door) * float(self.ticket_price_season)
        f_tot_t = f1_tot_t + f2_tot_t + sea_tot_t
        
        # Left justify
        serial_port.write(chr(0x1b) + chr(0x61) + chr(0x00))
        # Print
        serial_port.write(report_template.substitute(locals()))
        # Spool and cut
        if GetConfig("printer",'printer') == 'cbm':
            serial_port.write(chr(0x1b) + chr(0x64) + chr(0x03) + chr(0x1B) + chr(0x69))
        else:
            serial_port.write(chr(0x0C))
    
    def member_window(self, check_swipe):
        self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Check membership")
        self.ui.logbox.moveCursor(QtGui.QTextCursor.End)
        if self.memberWindow is None:
	    self.memberWindow = MemberChecker(self)
	self.memberWindow.check_free = check_swipe
	self.memberWindow.ui.lE_CID.setText("")
	self.memberWindow.ui.label_3.setEnabled(False)
	self.memberWindow.ui.lE_CID.setEnabled(False)
	self.memberWindow.ui.pB_CID.setEnabled(False)
	self.memberWindow.ui.lE_swipe.setText("")
	self.memberWindow.ui.lE_swipe.setFocus(True)
	
	variable = self.memberWindow.exec_()
	if variable == 0:
	    self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Person is not a member.")
	elif variable == 1:
	    if check_swipe == True:
	      self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Is member. Free ticket used.")
	    else:
              self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Is member. Free ticket not checked.")
        elif variable == 2:
	    self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": New member. Using free ticket.")
        elif variable == 3:
	    #Override case
            self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": OVERRIDE.")
            variable = 2
            # return value is set to 2, because anything other than 0 is success for non-free things, 
            # and 2 is success for free things.
        self.ui.logbox.moveCursor(QtGui.QTextCursor.End)
	
        print variable
        
        return variable
        
    
        
    def on_pB_Config_clicked(self,checked=None):
        if checked is None: return
        if self.configWindow is None:
            self.configWindow = TheConfigWindow(self)
            self.configWindow.ui.le_film1_name.setText(QtGui.QApplication.translate("ConfigWindow", GetConfig("film1",'title'), None, QtGui.QApplication.UnicodeUTF8))
            self.configWindow.ui.le_film2_name.setText(QtGui.QApplication.translate("ConfigWindow", GetConfig("film2",'title'), None, QtGui.QApplication.UnicodeUTF8))
            self.configWindow.ui.le_filmd_name.setText(QtGui.QApplication.translate("ConfigWindow", GetConfig("film1",'title') + ' and ' + GetConfig("film2",'title'), None, QtGui.QApplication.UnicodeUTF8))
            
            f1_time_h = int(GetConfig("film1",'time').split(':',1)[0])
            f1_time_m = int(GetConfig("film1",'time').split(':',2)[1])
            
            f2_time_h = int(GetConfig("film2",'time').split(':',1)[0])
            f2_time_m = int(GetConfig("film2",'time').split(':',2)[1])
            
            f1_price = GetConfig("film1",'mprice')
            f2_price = GetConfig("film2",'mprice')
            
            self.configWindow.ui.tE_film1_time.setTime(QtCore.QTime(f1_time_h,f1_time_m,0))
            self.configWindow.ui.tE_film2_time.setTime(QtCore.QTime(f2_time_h,f2_time_m,0))
            
            QtCore.QObject.connect(self.configWindow.ui.pB_commit,QtCore.SIGNAL("clicked()"), self.config_updated)
            
            QtCore.QObject.connect(self.configWindow.ui.le_film1_name,QtCore.SIGNAL("editingFinished()"), self.single_updated)
            QtCore.QObject.connect(self.configWindow.ui.le_film2_name,QtCore.SIGNAL("editingFinished()"), self.single_updated)
            
        self.configWindow.show()

    def single_updated(self):
      
      self.configWindow.ui.le_filmd_name.setText(QtGui.QApplication.translate("ConfigWindow", self.configWindow.ui.le_film1_name.text() + ' and ' + self.configWindow.ui.le_film2_name.text(), None, QtGui.QApplication.UnicodeUTF8))
        
    def config_updated(self):
      #print "update"

      film1_name = self.configWindow.ui.le_film1_name.text()
      film1_hour = self.configWindow.ui.tE_film1_time.time().hour()
      film1_minute = self.configWindow.ui.tE_film1_time.time().minute()
      film1_price = self.configWindow.ui.lE_price1.text()
      film1_price_mem = self.configWindow.ui.lE_price1_mem.text()
      
      film2_name = self.configWindow.ui.le_film2_name.text()
      film2_hour = self.configWindow.ui.tE_film2_time.time().hour()
      film2_minute = self.configWindow.ui.tE_film2_time.time().minute()
      film2_price = self.configWindow.ui.lE_price2.text()
      film2_price_mem = self.configWindow.ui.lE_price2_mem.text()
      
      filmd_price = self.configWindow.ui.lE_filmd_price_nonm.text()
      filmd_price_mem = self.configWindow.ui.lE_filmd_price.text()
      
      #print "film1", film1_name, film1_hour, film1_minute
      #print "film2", film2_name, film2_hour, film2_minute
      
      new_config = { "film1" : {"Title" : film1_name,
                                "Time" : str(film1_hour) + ":" + str(film1_minute),
                                "NPrice" : film1_price,
                                "MPrice" : film1_price_mem,
                                "FPrice" : "Free!",
                                "Showing" : "A",
                                "Special" : 0},
                     "film2" : {"Title" : film2_name,
                                "Time" : str(film2_hour) + ":" + str(film2_minute),
                                "NPrice" : film2_price,
                                "MPrice" : film2_price_mem,
                                "FPrice" : "Free!",
                                "Showing" : "B",
                                "Special" : "0"},
                     "doublebill" : {"Title" : film1_name + " and " + film2_name,
                                "Time" : str(film1_hour) + ":" + str(film1_minute),
                                "NPrice" : filmd_price,
                                "MPrice" : filmd_price_mem,
                                "FPrice" : "02.00",
                                "Showing" : "C",
                                "Special" : 0},
                     "season1" : {"Title" : film1_name,
                                "Time" : str(film1_hour) + ":" + str(film1_minute),
                                "MPrice" : "Season",
                                "Showing" : "D",
                                "Special" : 0},
                     "season2" : {"Title" : film2_name,
                                "Time" : str(film2_hour) + ":" + str(film2_minute),
                                "MPrice" : "Season",
                                "Showing" : "E",
                                "Special" : 0}}
                     
      SetConfig(new_config)
      
      self.ticket_price_non_mem_f1 = GetConfig("film1",'nprice')
      self.ticket_price_non_mem_f2 = GetConfig("film2",'nprice')
      
 
      self.ui.pB_film1_mem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\nMember price\n\n£" + str(GetConfig("film1",'mprice')), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_film1_nonmem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\nNon-Member price\n\n£" + str(self.ticket_price_non_mem_f1), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_film1_mem_first.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film1",'title') + "\n\n(Member, first film, FREE)\n\n£" + str(self.ticket_price_mem_free), None, QtGui.QApplication.UnicodeUTF8))
        
      self.ui.pB_film2_mem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\nMember price\n\n£"+str(GetConfig("film2",'mprice')), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_film2_nonmem.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\nNon-Member price\n\n£" + str(self.ticket_price_non_mem_f2), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_film2_mem_first.setText(QtGui.QApplication.translate("Form", "Ticket for " + GetConfig("film2",'title') + "\n\n(Member, first film, FREE)\n\n£" + str(self.ticket_price_mem_free), None, QtGui.QApplication.UnicodeUTF8))
      
      self.ui.pB_filmd_mem.setText(QtGui.QApplication.translate("Form", "Double Bill" + "\n\nMember price\n\n£"+str(GetConfig("doublebill",'mprice')), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_filmd_nonmem.setText(QtGui.QApplication.translate("Form", "Double Bill" + "\n\nNon-Member price\n\n£" + str(GetConfig("doublebill",'nprice')), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.pB_filmd_mem_first.setText(QtGui.QApplication.translate("Form", "Double Bill" + "\n\n(Member with film, FREE)\n\n£" + str(GetConfig("doublebill","fprice")), None, QtGui.QApplication.UnicodeUTF8))
      
      
      
      
      self.ui.logbox.appendPlainText(str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())) + ": Config updated.")
      self.ui.logbox.moveCursor(QtGui.QTextCursor.End)
    
      self.configWindow.close()
    
    def on_pB_season1_clicked(self,checked=None):
      if checked is None: return
      
      if self.member_window(False):
        self.ticket_cnt_total = self.ticket_cnt_total + 1
        
        self.ticket_cnt_season_f1 = self.ticket_cnt_season_f1 + 1

        self.ticket_cnt_season = self.ticket_cnt_season + 1
        
        self.people_cnt_film1 = self.people_cnt_film1 + 1
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_season1.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_f1), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('season1','m',self.ticket_cnt_total)
        
    def on_pB_season2_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(False):
        self.ticket_cnt_total = self.ticket_cnt_total + 1
        
        self.ticket_cnt_season_f2 = self.ticket_cnt_season_f2 + 1

        self.ticket_cnt_season = self.ticket_cnt_season + 1
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_season2.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_f2), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('season2','m',self.ticket_cnt_total)
        
    def on_pB_season_door_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(False):
        self.ticket_cnt_season_door = self.ticket_cnt_season_door + 1
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_season_door.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_season_door), None, QtGui.QApplication.UnicodeUTF8))
        
        
        
    def on_pB_film1_mem_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(False):
        self.cash_drawer_open()
        
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_f1_mem = self.ticket_cnt_f1_mem + 1
        
        self.people_cnt_film1 = self.people_cnt_film1 + 1
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_film1_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f1_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('film1','m',self.ticket_cnt_total)
        
        
    def on_pB_film2_mem_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(False):
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_f2_mem = self.ticket_cnt_f2_mem + 1
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_film2_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('film2','m',self.ticket_cnt_total)
        
    def on_pB_filmd_mem_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(False):
        
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_db_mem = self.ticket_cnt_db_mem + 1
        
        self.people_cnt_film1 = self.people_cnt_film1 + 1
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_db_mem.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_mem), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('doublebill','m',self.ticket_cnt_total)
        
    def on_pB_film1_nonmem_clicked(self,checked=None):
      if checked is None: return
        
      self.ticket_cnt_total = self.ticket_cnt_total + 1

      self.ticket_cnt_f1_nonm = self.ticket_cnt_f1_nonm + 1
      
      self.people_cnt_film1 = self.people_cnt_film1 + 1
      self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
      
      self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.ct_film1_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f1_nonm), None, QtGui.QApplication.UnicodeUTF8))

      # Print the ticket.
      print_ticket('film1','n',self.ticket_cnt_total)
        
        
    def on_pB_film2_nonmem_clicked(self,checked=None):
        if checked is None: return
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_f2_nonm = self.ticket_cnt_f2_nonm + 1
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_film2_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_nonm), None, QtGui.QApplication.UnicodeUTF8))

        # Print the ticket.
        print_ticket('film2','n',self.ticket_cnt_total)
        
    def on_pB_filmd_nonmem_clicked(self,checked=None):
      if checked is None: return
      self.ticket_cnt_total = self.ticket_cnt_total + 1

      self.ticket_cnt_db_nonm = self.ticket_cnt_db_nonm + 1
      
      self.people_cnt_film1 = self.people_cnt_film1 + 1
      self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
      
      self.people_cnt_film2 = self.people_cnt_film2 + 1
      self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
      
      self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
      self.ui.ct_db_nonm.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_nonm), None, QtGui.QApplication.UnicodeUTF8))

      # Print the ticket.
      print_ticket('doublebill','n',self.ticket_cnt_total)
        
    def on_pB_film1_mem_first_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(True) == 2:
        
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_f1_free = self.ticket_cnt_f1_free + 1
        
        self.people_cnt_film1 = self.people_cnt_film1 + 1
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_film1_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f1_free), None, QtGui.QApplication.UnicodeUTF8))
        
        # Print the ticket.
        print_ticket('film1','f',self.ticket_cnt_total)
        
    def on_pB_film2_mem_first_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(True) == 2:
       
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_f2_free = self.ticket_cnt_f2_free + 1
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_film2_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_f2_free), None, QtGui.QApplication.UnicodeUTF8))

        # Print the ticket.
        print_ticket('film2','f',self.ticket_cnt_total)
        
    def on_pB_filmd_mem_first_clicked(self,checked=None):
      if checked is None: return
      if self.member_window(True) == 2:
        self.ticket_cnt_total = self.ticket_cnt_total + 1

        self.ticket_cnt_db_free = self.ticket_cnt_db_free + 1
        
        self.people_cnt_film1 = self.people_cnt_film1 + 1
        self.ui.people_film1.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film1), None, QtGui.QApplication.UnicodeUTF8))
        
        self.people_cnt_film2 = self.people_cnt_film2 + 1
        self.ui.people_film2.setText(QtGui.QApplication.translate("Form", str(self.people_cnt_film2), None, QtGui.QApplication.UnicodeUTF8))
        
        self.ui.ct_total.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_total), None, QtGui.QApplication.UnicodeUTF8))
        self.ui.ct_db_memf.setText(QtGui.QApplication.translate("Form", str(self.ticket_cnt_db_free), None, QtGui.QApplication.UnicodeUTF8))

        
        # Print the ticket.
        print_ticket('doublebill','f',self.ticket_cnt_total)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = StartMainGUI()
    myapp.showMaximized()
    sys.exit(app.exec_())
