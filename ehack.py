#!/usr/bin/env python
#
#
#

import urllib
import httplib2


# eHack
# 
# Download an item from eActivities.
# You must provide:
# 
# username/password of a user authorised to access the document (strings)
# URL of the page the document appears on (as far as possible) (string)
# IDs of the next steps to downloading the document (nav, a list)
# ID of the document you want
# type of the document you want
# filename of the document you want

# Tested:
# eHack(*, *, "/admin/csp/details", [3, 395], 1700, "csv", "Members_report")


def eHack(username, password, page, nav, id, type, filename):
    headers = {'Content-type': 'application/x-www-form-urlencoded'}
    http = httplib2.Http()
    
    
    loginform = urllib.urlencode({"ajax":"login","name":username,"pass":password,"objid":"1"})
    
    # Make login request
    response2, content = http.request("https://eactivities.union.ic.ac.uk\
/common/ajax_handler.php", 'POST', body=loginform, headers=headers)
    
    
    # We now get a cookie, which we must keep safe until we're done.
    thestring = response2['set-cookie']
    ending = "; path=/"
    if thestring.endswith(ending):
        thestring =  thestring[:-len(ending)]
    
    headers['Cookie'] = thestring
    
    #print content
    
    # Go to admin/csp/details page
    response2, content2 = http.request("https://eactivities.union.ic.ac.uk" + page, 'POST', headers=headers)
    
    #print response2
    #print content2
    
    # Go to "members" and download the report.
    
    form =  urllib.urlencode({"ajax":"setup", "navigate":nav[0]})
    response2, content = http.request("https://eactivities.union.ic.ac.uk/common/ajax_handler.php", 'POST', body=form, headers=headers)
    

    navremain = nav[1:]
    for navigate in navremain:
        #print navigate
        form =  urllib.urlencode({"ajax":"activatetabs", "navigate":str(navigate)})
        #print form
        response2, content = http.request("https://eactivities.union.ic.ac.uk/common/ajax_handler.php", 'POST', body=form, headers=headers)
        #print response2

    
    # Report download here.
    response2, content = http.request("https://eactivities.union.ic.ac.uk\
/common/data_handler.php?id="+str(id)+"&type="+type+"&name="+filename, 'POST', headers=headers)
    
    return content

