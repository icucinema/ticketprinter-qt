#!/usr/bin/env python

# Test script to import members

import sqlite3


def import_members(database, new_members):
    # new_members is the mess from eactivities - just a csv, really
    database.text_factory = str
    errors_count = 0
    existing_members_count = 0
    new_members_count = 0
    
    members_list = new_members.split('\r\n')
    
    #print new_members
    for member in members_list[2:-1]:
        member_elements =  member.split(',')
        try:
            new_cid = member_elements[2]
            new_login = member_elements[3]
            #print new_cid, new_login
            db_conn = database.cursor()
            
            i=0
            for member in db_conn.execute('SELECT * FROM members WHERE (cid=' + new_cid + ' AND login=' + new_login + ')'):
                i += 1
                existing_members_count += 1
                #print "member", i, member
                # Already a member, so nothing to do
            
            if i != 1:
                #print 'not member'
                new_members_count += 1
                # new member, so add!
                for j in range(0,7): ## EACTIVITIES IS SHIT
                    member_elements[j] = member_elements[j].strip('"')
                #print member_elements
                db_conn.execute('INSERT INTO members (start_date, order_id, cid, login, firstname, lastname, email) VALUES (?,?,?,?,?,?,?)', member_elements)
                database.commit()
                #for j in db_conn.execute('SELECT * FROM members WHERE (cid=' + new_cid + ' AND login=' + new_login + ')'):
                 #   print j
                
        except IndexError:
            print 'No CID and Login for member', member_elements
            errors_count += 1
            # Probably a blank line or some shit.
            
    #print 'errors:', errors_count
    #print 'existing:', existing_members_count
    #print 'new:', new_members_count
    return 'Errors: ' + str(errors_count) + '\nExisting: ' + str(existing_members_count) + '\nNew: ' + str(new_members_count)
    
def maint():
    
    file_name = '' + "boxoffice.db"
    db = sqlite3.connect(file_name)
    

    
    f = open('Members_report.csv', 'r')
    
    import_members(db, f.read())
    db.commit()
    db.close()

